#include <iostream>
#include <unistd.h>
#include <iomanip>
#include <cstdint>

int currentIndex=-1;

template<typename T>
T readType(uint8_t* buffer,bool isVar = false) {
    int size = sizeof(T);
    T result = 0;

    if (isVar) {
        for (int i=size; i>=0; i--) {
            result += buffer[currentIndex + i] << ((i-1) * 8);
        }
    } else {
        for (int i=1; i<=size; i++) {
            result += buffer[currentIndex + i] << ((size-i) * 8);
        }
    }

    currentIndex+=size;

    return result;
}

int readVarInt(uint8_t* buffer) {
    int value = 0;
    int position = 0;
    uint8_t currentByte;

    while (true) {
        currentByte = readType<uint8_t>(buffer,true);
        value |= (currentByte & 0x7F) << position;

        if ((currentByte & 0x80) == 0) break;

        position += 7;

        if (position >= 32) throw new std::overflow_error("VarInt is too big");
    }

    return value;
}

long readVarLong(uint8_t* buffer) {
    long value = 0;
    int position = 0;
    uint8_t currentByte;

    while (true) {
        currentByte = readType<uint8_t>(buffer,true);
        value |= (long) (currentByte & 0x7f) << position;

        if ((currentByte & 0x80) == 0) break;

        position += 7;

        if (position >= 64) throw new std::overflow_error("VarLong is too big");
    }

    return value;
}

int* readPacketInfo(uint8_t* buffer) {
    int packetId = readVarInt(buffer);
    int packetLength = readVarInt(buffer);
    return new int[2]{packetId, packetLength};
}

char* readString(uint8_t* buffer) {
    int length=readVarInt(buffer);

    char *result = (char*)malloc(sizeof(char) * length);

    for (int i=0;i<length;i++) {
        result[i] = readType<uint8_t>(buffer,true);
    }

    result[length]='\0';

    return result;
}


void respond(int serverfd, int clientfd) {
    uint8_t buffer[2048] = {0};
    int dataNum;

    do {
        dataNum = read(clientfd,buffer,2048);
        
        /*std::cout << std::hex << std::setfill('0') << std::setw(2);
        for (int i=0;i<dataNum;i++) {
            std::cout << (int)buffer[i] << "\t" << buffer[i] << std::endl;
        }

        std::cout << std::dec << std::setw(0) << std::setfill('\0') << std::endl;*/

        int* packetInfo=readPacketInfo(buffer);
        int length = packetInfo[0];
        int packetId = packetInfo[1];

        std::cout << "Packet ID: " << packetId << std::endl;
        std::cout << "Packet length: " << length << std::endl;

        switch (packetId) {
            case 0x00:
            {
                std::cout << "Protocol version: " << readVarInt(buffer) << std::endl;
                char* hostname = readString(buffer);
                std::cout << "Hostname: " << hostname << std::endl;
                free(hostname);
                std::cout << "Server Port: " << (int)readType<unsigned short>(buffer) << std::endl;
                std::cout << "Next State: " << readVarInt(buffer) << std::endl;
                break;
            }
            default:
                std::cout << "Unknown packet type with ID " << packetId << std::endl;
                break;
        }
    } while (dataNum <= 0);
}