#include <iostream>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <iomanip>
#include "server.h"

#define PORT 25565

void stopServer(int serverfd, int clientfd) {
    close(clientfd);
    shutdown(serverfd,SHUT_RDWR);
}

int* startServer() {
    int opt = 1;
    int serverfd, clientfd;

    struct sockaddr_in address;
    int addrlen = sizeof(address);

    if ((serverfd = socket(AF_INET,SOCK_STREAM,0)) < 0) {
        std::cerr << "socket creation failure" << std::endl;
        return new int[0]{};
    }

    if (setsockopt(serverfd,SOL_SOCKET,SO_REUSEADDR | SO_REUSEPORT,&opt,sizeof(opt)) < 0) {
        std::cerr << "setsockopt failure" << std::endl;
        return new int[0]{};
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    if (bind(serverfd,(struct sockaddr*)&address,sizeof(address)) < 0) {
        std::cerr << "bind failure" << std::endl;
        return new int[0]{};
    }

    if (listen(serverfd,2) < 0) {
        std::cerr << "listen failure" << std::endl;
        return new int[0]{};
    }

    std::cout << "Server is accepting connections..." << std::endl;

    if ((clientfd = accept(serverfd,(struct sockaddr*)&address,(socklen_t*)&addrlen)) < 0) {
        std::cerr << "accept failure" << std::endl;
        return new int[0]{};
    }

    return new int[2]{serverfd,clientfd};
}

int main() {

    int* fds=startServer();
    
    if (!fds[0]) return -1;

    int serverfd = fds[0];
    int clientfd = fds[1];

    respond(serverfd, clientfd);
    stopServer(serverfd,clientfd);

    return 0;
}

